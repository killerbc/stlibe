<?php
/**
 *  模块名称
 *  Author: zhanglei
 *  Email: 83344873@qq.com
 *  Date: 2021/8/9 17:33
 */

namespace StLib\ServiceInterface;

interface AbstractServiceInterface
{
    /**
     * 添加数据
     * date 2021/5/28 14:32:52
     * @author zhanglei <83344873@qq.com>
     * @param array $data
     * @return array
     */
    public function add(array $data): array;

    /**
     * 编辑数据
     * date 2021/5/28 14:33:00
     * @author zhanglei <83344873@qq.com>
     * @param array $data
     * @return array
     */
    public function edit(array $data): array;

    /**
     * 删除数据
     * date 2021/5/28 14:33:11
     * @param array $data
     * @return array
     * @author zhanglei <83344873@qq.com>
     */
    public function delete(array $data): array;

    /**
     * 通过id获取单条信息
     * date 2021/5/28 14:33:25
     * @param array $data
     * @return array
     * @author zhanglei <83344873@qq.com>
     */
    public function getInfoById(array $data): array;

    /**
     * 通过where条件获取单条信息
     * date 2021/5/28 14:33:46
     * @author zhanglei <83344873@qq.com>
     * @param array $where
     * @return array
     */
    public function getInfoByWhere(array $where): array;

    /**
     * 通过条件获取列表信息
     * date 2021/5/28 14:34:07
     * @author zhanglei <83344873@qq.com>
     * @param array $where
     * @param string $page
     * @param string $pageSize
     * @return array
     */
    public function getList(array $where, string $page = '', string $pageSize = ''): array;
}