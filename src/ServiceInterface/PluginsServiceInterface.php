<?php
/**
 *  模块名称
 *  Author: zhanglei
 *  Email: 83344873@qq.com
 *  Date: 2021/8/9 17:33
 */

namespace StLib\ServiceInterface;

interface PluginsServiceInterface extends AbstractServiceInterface
{
    /**
     * 获取ID
     * date 2021/5/11 16:14:19
     * @author zhanglei <83344873@qq.com>
     * @param int $num
     * @return array
     */
    public function getId(int $num=1): array;

    /**
     * 根据ip地址获取位置信息
     * date 2021/5/10 18:21:08
     * @author zhanglei <83344873@qq.com>
     * @param string $ip
     * @return array
     */
    public function ipToArea(string $ip): array;

    /**
     * 获取字符串首字母
     * date 2021/5/11 10:28:15
     * @param string $string
     * @return array
     * @author zhanglei <83344873@qq.com>
     */
    public function getFirstChar(string $string): array;
}